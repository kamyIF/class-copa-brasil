let Times = [];

function add() {
  let nome = document.getElementById("nomeDoTime").value;
  Times.push(nome);
  formarTimes();
  placares();
}

function formarTimes() {
  let select1 = document.getElementById("select1");
  let select2 = document.getElementById("select2");

  select1.innerHTML = "";
  select2.innerHTML = "";

  for (let i of Times) {
    let option1 = document.createElement("option");
    let option2 = document.createElement("option");
    option1.text = i;
    option2.text = i;
    select1.add(option1);
    select2.add(option2);
  }
}

function parteDoJogo() {
  const input = '<input type="text">';
  let jogo = input + "X" + input;
  jogo += formarTimes();
  return jogo;
}


function placares() {
  const comJogo = parteDoJogo();
  let placar = "";
  placar += comJogo + "<br>";
  let div = document.getElementById("placar");
  div.innerHTML = placar;
}

function apresentar() {
  let select1 = document.getElementById("select1");
  let select2 = document.getElementById("select2");
  let v1 = select1.options[select1.selectedIndex].text;
  let v2 = select2.options[select2.selectedIndex].text;
  let output = v1 + " X " + v2;
  document.getElementById("saída").innerHTML = output;
}

placares();

